<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\BookingController;
use App\Http\Middleware\CheckToken;
use App\Http\Controllers\SeatController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('register',[AuthController::class,'register']);
Route::post('login',[AuthController::class,'login']);
Route::get('airport',[SearchController::class,'airports']);
Route::get('flight',[SearchController::class,'flights']);
Route::get('booking/{code}',[BookingController::class,'show']);
Route::post('booking',[BookingController::class,'create']);
Route::get('booking/{code}/seat',[SeatController::class,'occupied']);
Route::patch('booking/{code}/seat',[SeatController::class,'change']);

Route::middleware([CheckToken::class])->group(function () {
    Route::get('user',[AuthController::class,'userInfo']);
    Route::get('user/booking',[BookingController::class,'userBookings']);
});
