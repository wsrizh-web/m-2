<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="icon" href="/public/favicon.ico">
    <title>m-3-proj</title>
    <link href="/public/css/app.6a13336f.css" rel="preload" as="style">
    <link href="/public/js/app.e6319e85.js" rel="preload" as="script">
    <link href="/public/js/chunk-vendors.d78b22e3.js" rel="preload" as="script">
    <link href="/public/css/app.6a13336f.css" rel="stylesheet">
</head>
<body>
<noscript><strong>We're sorry but m-3-proj doesn't work properly without JavaScript enabled. Please enable it to
        continue.</strong></noscript>
<div id="app"></div>
<script src="/public/js/chunk-vendors.d78b22e3.js"></script>
<script src="/public/js/app.e6319e85.js"></script>
</body>
</html>
