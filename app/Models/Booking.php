<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;

    protected $fillable = ['flight_from', 'flight_back', 'date_from', 'date_back', 'code'];


    public function flightFrom()
    {
        return $this->belongsTo(Flight::class, 'flight_from');
    }

    public function flightBack()
    {
        return $this->belongsTo(Flight::class, 'flight_back');
    }

    public function passengers()
    {
        return $this->hasMany(Passenger::class, 'booking_id');
    }

    public function getFlightsAttribute()
    {
        return [$this->flightFrom, $this->flightBack];
    }

    public function scopeCheckOverload($query, $bookingPassengersCount)
    {
        $flightPassengersCount = $query->withCount('passengers')->get()->sum('passengers_count');
        $passengersTotal = $flightPassengersCount + $bookingPassengersCount;
        return $passengersTotal > 48;
    }
}
