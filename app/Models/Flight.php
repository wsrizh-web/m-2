<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
    use HasFactory;

    public function from() {
        return $this->belongsTo(Airport::class,'from_id');
    }
    public function to() {
        return $this->belongsTo(Airport::class,'to_id');
    }

    public function getTimeFromAttribute()
    {
        return $this->formatTime($this->attributes['time_from']);
    }

    public function getTimeToAttribute()
    {
        return $this->formatTime($this->attributes['time_to']);
    }

    protected function formatTime($attr) {
        return Carbon::createFromTimeString($attr)->format('H:i');
    }
}
