<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->bearerToken();

        $user = User::query()->whereNotNull('api_token')
            ->where('api_token', $token)->first();

        if ($user) {
            Auth::loginUsingId($user->id);
            return $next($request);
        } else {
            return response()->json([
                'error'=>[
                    'code'=>401,
                    'message'=>'Unauthorized',
                ]
            ],401);
        }
    }
}
