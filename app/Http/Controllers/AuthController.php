<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;


class AuthController extends Controller
{
    public function register(Request $request)
    {

        $validate = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'phone' => 'required|string|unique:users,phone',
            'document_number' => 'required|digits:10',
            'password' => 'required|string',
        ], [
            'document_number.digits'=>'Номер документа должен состоять из 10 цифр.'
        ]);

        if ($validate->fails()) {
            return $this->valid('Validation error', $validate->errors(), 422);
        } else {
            User::query()->create([
                'first_name' => $request->input('first_name'),
                'last_name' => $request->input('last_name'),
                'phone' => $request->input('phone'),
                'document_number' => $request->input('document_number'),
                'password' => $request->input('password')
            ]);
            return response(null, 204);
        }
    }

    public function login(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'phone' => 'required',
            'password' => 'required'
        ]);

        if ($validate->fails()) {
            return $this->valid('Validation error', $validate->errors(), 422);
        } else {
            $user = User::query()->where([
                'phone' => $request->input('phone'),
                'password' => $request->input('password'),
            ])->first();

            if ($user) {
                $user->api_token = Hash::make(Str::random(12));
                $user->save();
                return response()->json([
                    'data' => [
                        'token' => $user->api_token
                    ]
                ], 200);
            } else {
                return response()->json([
                    'error' => [
                        'code' => 401,
                        'message' => 'Unauthorized',
                        'errors' => [
                            'phone' => ['phone or password incorrect']
                        ]
                    ]
                ], 401);
            }
        }
    }

    public function userInfo()
    {
        $user = User::query()->where('id', Auth::id())
            ->select('first_name', 'last_name', 'phone', 'document_number')
            ->first();

        return response()->json($user, 200);
    }
}
