<?php

namespace App\Http\Controllers;

use App\Models\Airport;
use App\Models\Booking;
use App\Models\Flight;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SearchController extends Controller
{
    public function airports(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'query' => 'required',
        ]);
        if ($validate->fails()) {
            return $this->valid('Validation error', $validate->errors(), 422);
        }

        $query = $request->get('query');
        $airports = Airport::query()->where('city', 'like', "%$query%")
            ->orWhere('name', 'like', "%$query%")
            ->orWhere('iata', 'like', "%$query%")
            ->select('name', 'iata')->get();
        return response()->json([
            'data' => [
                'items' => $airports
            ]
        ], 200);

    }

    public function flights(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'from' => 'required',
            'to' => 'required',
            'date1' => 'required|date_format:Y-m-d',
            'date2' => 'date_format:Y-m-d|after_or_equal:date1',
            'passengers' => 'required|integer|min:1|max:8',
        ],[
            'from.required'=>'Аэропорт вылета не может быть пустым.',
            'to.required'=>'Аэропорт прилета не может быть пустым.',
            'date1.required'=>'Дата вылета обязательна для заполнения.',
            'date2.after_or_equal'=>'Дата вылета обратно не может быть раньше даты вылета.',
        ]);

        if ($validate->fails()) {
            return $this->valid('Validation error', $validate->errors(), 422);
        } else {
            $flightsTo = Flight::query()
                ->with(['from', 'to'])
                ->whereHas('from', function ($q) use ($request) {
                    $q->where('iata', $request->get('from'));
                })
                ->whereHas('to', function ($q) use ($request) {
                    $q->where('iata', $request->get('to'));
                })
                ->get();

            $flightsTo->each(function ($flight, $key) use ($request, $flightsTo) {
                $flight->unsetRelation('from')->unsetRelation('to');
                $overload = Booking::query()
                    ->where([
                        ['date_from', $request->input('date1')],
                        ['flight_from', $flight->id]
                    ])
                    ->checkOverload($request->input('passengers'));
                if ($overload) $flightsTo->splice($key, 1);

                $flight->flight_id = $flight->id;

                $flight->from->airport = $flight->from->name;
                $flight->from->date = $request->get('date1');
                $flight->from->time = $flight->time_from;
                $flight->to->airport = $flight->to->name;
                $flight->to->date = $request->get('date1');
                $flight->to->time = $flight->time_to;

                $flight->makeHidden('id');
                $flight->from->makeHidden('id', 'created_at', 'updated_at', 'name');
                $flight->to->makeHidden('id', 'created_at', 'updated_at', 'name');
            })
                ->makeHidden(['from_id', 'to_id', 'time_from', 'time_to', 'created_at', 'updated_at']);

            $flightsBack = [];

            if ($request->has('date2')) {
                $flightsBack = Flight::query()
                    ->with(['from', 'to'])
                    ->whereHas('from', function ($q) use ($request) {
                        $q->where('iata', $request->get('to'));
                    })
                    ->whereHas('to', function ($q) use ($request) {
                        $q->where('iata', $request->get('from'));
                    })
                    ->get();

                $flightsBack->each(function ($flight, $key) use ($request, $flightsBack) {
                    $flight->unsetRelation('from')->unsetRelation('to');
                    $overload = Booking::query()
                        ->where([
                            ['date_back', $request->input('date2')],
                            ['flight_back', $flight->id]
                        ])
                        ->checkOverload($request->input('passengers'));

                    if ($overload) $flightsBack->splice($key, 1);

                    $flight->flight_id = $flight->id;
                    $flight->from->date = $request->get('date2');
                    $flight->from->time = Carbon::createFromTimeString($flight['time_from'])->format('H:i');
                    $flight->to->date = $request->get('date2');
                    $flight->to->time = Carbon::createFromTimeString($flight['time_to'])->format('H:i');

                    $flight->makeHidden('id');
                    $flight->from->makeHidden('id', 'created_at', 'updated_at');
                    $flight->to->makeHidden('id', 'created_at', 'updated_at');
                })
                    ->makeHidden(['from_id', 'to_id', 'time_from', 'time_to', 'created_at', 'updated_at']);
            }


            return response()->json([
                'data' => [
                    'flights_to' => $flightsTo,
                    'flights_back' => $flightsBack
                ]
            ], 200);
        }
    }
}
