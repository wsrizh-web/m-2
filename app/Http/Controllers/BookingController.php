<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Passenger;
use Illuminate\Http\Request;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;


class BookingController extends Controller
{
    protected function printFlightsInfo($bookings)
    {
        foreach ($bookings as $booking) {
            $booking->append('flights');
            $bookingCost = 0;

            foreach ($booking->flights as $flight) {
                $flightCost = $flight->cost;
                $bookingCost += $flightCost * count($booking->passengers);

                $flight->flight_id = $flight->id;
                $flight->from->airport = $flight->from->name;
                $flight->from->date = $booking->date_from;
                $flight->from->time = $flight->time_from;
                $flight->to->airport = $flight->to->name;
                $flight->to->date = $booking->date_back;
                $flight->to->time = $flight->time_to;

                $flight->makeHidden('id', 'from_id', 'to_id', 'created_at', 'updated_at', 'time_from', 'time_to');
                $flight->from->makeHidden('id', 'created_at', 'updated_at', 'name');
                $flight->to->makeHidden('id', 'created_at', 'updated_at', 'name');
            }
            foreach ($booking->passengers as $passenger)
                $passenger->makeHidden('booking_id');


            $booking->makeHidden([
                'id',
                'created_at',
                'updated_at',
                'date_from',
                'date_back',
                'flight_from',
                'flightFrom',
                'flight_back',
                'flightBack'
            ]);
            $booking->cost = $bookingCost;
        }

        return $bookings;
    }

    public function show(Request $request, $code)
    {
        $booking = Booking::with('passengers')
            ->where('code', $code)
            ->first();

        if (!$booking) {
            return $this->valid('Booking is not exist', null, 404);
        }
        $booking = $this->printFlightsInfo([$booking]);

        return response()->json([
            'data' => $booking[0]
        ], 200);
    }

    public function create(Request $request, Faker $faker)
    {
        $validate = Validator::make($request->all(), [
            'flight_from.id' => 'required',
            'flight_back.id' => 'required',
            'flight_from.date' => 'required|date_format:Y-m-d',
            'flight_back.date' => 'date_format:Y-m-d',
            'passengers' => 'array|between:1,8',
            'passengers.*.first_name' => 'required',
            'passengers.*.last_name' => 'required',
            'passengers.*.document_number' => 'required|digits:10',
            'passengers.*.birth_date' => 'required|date_format:Y-m-d'
        ]);

        if ($validate->fails()) {
            return $this->valid('Validation error', $validate->errors(), 422);
        } else {
            $passengersCount = count($request->input('passengers'));

            $overloadFrom = Booking::query()
                ->where([
                    ['date_from', $request->input('flight_from.date')],
                    ['flight_from', $request->input('flight_from.id')],
                ])
                ->checkOverload($passengersCount);

            $overloadBack = Booking::query()
                ->where([
                    ['date_back', $request->input('flight_back.date')],
                    ['flight_back', $request->input('flight_back.id')]
                ])
                ->checkOverload($passengersCount);

            if ($overloadFrom || $overloadBack) {
                return response()->json([
                    'data' => [
                        'code' => 422,
                        'message' => 'All seats is occupied'
                    ]
                ], 422);
            }

            $bookingCode = $faker->lexify('?????');
            $bookingCode = Str::upper($bookingCode);

            $booking = Booking::create([
                'flight_from' => $request->input('flight_from.id'),
                'flight_back' => $request->input('flight_back.id'),
                'date_from' => $request->input('flight_from.date'),
                'date_back' => $request->input('flight_back.date'),
                'code' => $bookingCode
            ]);
            $booking->passengers()->createMany($request->input('passengers'));

            return response()->json([
                'data' => [
                    'code' => $bookingCode
                ]
            ], 201);
        }
    }

    public function userBookings()
    {
        $passengerBookings = Passenger::query()
            ->where('document_number', Auth::user()->document_number)
            ->pluck('booking_id');

        $bookings = Booking::query()
            ->with('passengers')
            ->find($passengerBookings);


        $bookings = $this->printFlightsInfo($bookings);


        return $bookings;
    }
}
