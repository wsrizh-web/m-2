<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Passenger;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class SeatController extends Controller
{
    protected function flightSeats($bookings, $backDirection)
    {
        $occupiedFrom = [];

        foreach ($bookings as $booking) {
            foreach ($booking->passengers as $passenger) {
                $place = $backDirection ? $passenger->place_back : $passenger->place_from;

                if ($place) {
                    $occupiedFrom[] = [
                        'passenger_id' => $passenger->id,
                        'place' => $place
                    ];
                }
            }
        }

        return $occupiedFrom;
    }

    public function occupied(Request $request)
    {
        $booking = Booking::where('code', $request->code)->first();

        $flightFromBookings = Booking::with('passengers')
            ->where([
                ['flight_from', $booking->flight_from],
                ['date_from', $booking->date_from]
            ])
            ->get();

        $flightBackBookings = Booking::with('passengers')
            ->where([
                ['flight_back', $booking->flight_back],
                ['date_back', $booking->date_back]
            ])
            ->get();

        $occupiedFrom = $this->flightSeats($flightFromBookings, false);

        $occupiedBack = $this->flightSeats($flightBackBookings, true);

        if ($request->method()==='PATCH') {
            $checkedArr = $request->type === 'from' ? $occupiedFrom : $occupiedBack;
            $checkedArr = array_map(function ($passenger) {
                return $passenger['place'];
            }, $checkedArr);

            return in_array($request->seat, $checkedArr);
        } else {
            return response()->json([
                "data" => [
                    "occupied_from" => $occupiedFrom,
                    "occupied_back" => $occupiedBack
                ]
            ], 200);
        }
    }

    public function change(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'passenger' => 'required',
            'seat' => 'required',
            'type' => [
                'required',
                Rule::in(['from', 'back']),
            ],
        ]);

        if ($validate->fails()) {
            return $this->valid('Validation error', $validate->errors(), 422);
        } else {
            $inBooking = Booking::where('code', $request->code)
                ->first()
                ->passengers()
                ->find($request->input('passenger'));

            if (!$inBooking) {
                return response()->json([
                    'error' => [
                        'code' => 403,
                        'message' => 'Passenger does not apply to booking'
                    ]
                ], 403);
            }

            $occupied = $this->occupied($request);

            if ($occupied) {
                return response()->json([
                    'error' => [
                        'code' => 422,
                        'message' => 'Seat is occupied'
                    ]
                ], 422);
            }

            $directionField = $request->input('type') === 'from' ? 'place_from' : 'place_back';

            $passenger = Passenger::find($request->input('passenger'));
            $passenger[$directionField] = $request->input('seat');
            $passenger->save();

            return $passenger->makeHidden('created_at', 'updated_at');
        }
    }
}
